<?php

// require browser detection script
require_once(PLUGIN_DIR . '/vendor/BrowserDetection.php');

class VolcannoDebugInfo {
    
    private $browser = '';
    public $user_id = ''; 
    public $user_agent = '';
    public $user_IP = '';

    public function init(){

        $this->browser = new Wolfcast\BrowserDetection();
        $this->user_id = get_current_user_id();
        $this->user_agent = $this->browser->getUserAgent();
        $this->user_IP = $this->get_user_ip();
        $this->add_debug_info();
    }

    public function set_browser_detect($user_agent){

        $this->browser = new Wolfcast\BrowserDetection();
        $this->browser->setUserAgent($user_agent);
    
        return array(
            'userAgent'       => $this->browser->getUserAgent(),               //string
            'browserName'     => $this->browser->getName(),                    //string
            'browserVer'      => $this->browser->getVersion(),                 //string
            'platformFamily'  => $this->browser->getPlatform(),                //string
            'platformVer'     => $this->browser->getPlatformVersion(true),     //string
            'platformName'    => $this->browser->getPlatformVersion(),         //string
            'platformIs64bit' => $this->browser->is64bitPlatform(),            //boolean
            'isMobile'        => $this->browser->isMobile(),                   //boolean
            //'isRobot'         => $browser->isRobot(),                    //boolean
            //'isInIECompat'    => $browser->isInIECompatibilityView(),    //boolean
            //'strEmulatedIE'   => $browser->getIECompatibilityView(),     //string
            //'arrayEmulatedIE' => $browser->getIECompatibilityView(true), //array('browser' => '', 'version' => '')
            //'isChromeFrame'   => $browser->isChromeFrame()              //boolean
        );
    }

    public function get_user_ip(){

        if (!empty($_SERVER["HTTP_CLIENT_IP"])){

            //check for ip from share internet
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){

            // Check for the Proxy User
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        else{

            $ip = $_SERVER["REMOTE_ADDR"];
        }
        // This will print user's real IP Address
        // does't matter if user using proxy or not.
        return $ip;
    }

    public function add_debug_info(){

        if($update_post_id = $this->check_if_debug_info_exist()){

            $post_meta = (array) get_post_meta( $update_post_id, "valcanno_debug_info", true );
            
            if($post_meta[$this->user_IP]['user_agent'] == $this->user_agent)
                return;
                
            $post_meta[$this->user_IP] = array(
                'user_agent' => $this->user_agent,
                'time' => current_time( 'mysql' ),
                'country_code' => isset($_SERVER["HTTP_CF_IPCOUNTRY"]) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : "-",
                'debug' => "update_post_meta"
            );

            update_post_meta( $update_post_id, "valcanno_debug_info", $post_meta );
        }else{

            if(wp_get_current_user()->ID == 0)
                return;
                
            $new_post_id = wp_insert_post( array(
                'post_type' => POSTTYPE,
                'post_status' => 'publish',
                'post_title' => wp_get_current_user()->display_name . " - ID: " . wp_get_current_user()->ID,
            ) );

            $meta[$this->user_IP] = array(
                'user_agent' => $this->user_agent,
                'time' => current_time( 'mysql' ),
                'country_code' => isset($_SERVER["HTTP_CF_IPCOUNTRY"]) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : "-",
                'debug' => "else"
            );

            update_post_meta( $new_post_id, "valcanno_debug_info", $meta );
        }
    }

    /**
     *  check if debug info for current user exist
     */
    public function check_if_debug_info_exist(){

        $debug_info_posts = get_posts( array( 'post_type' => POSTTYPE, 'numberposts' => -1));
        foreach( $debug_info_posts as $post ) {

            if($post->post_author == $this->user_id){
                return $post->ID;
            }
        }

        return false;
    }

    /**
     * VET log
     */
    public function add_error_tracker_log($errors){

        if($update_post_id = $this->check_if_debug_info_exist()){

            $post_meta = (array) get_post_meta( $update_post_id, "valcanno_debug_info", true );
            
           /*  if($post_meta[$this->user_IP]['user_agent'] == $this->user_agent)
                return; */

            $post_meta[$this->user_IP]['logs'] = array(
                'console_errors' => $errors,
                'debug' => "update_console_errors"
            );

            update_post_meta( $update_post_id, "valcanno_debug_info", $post_meta );

            return $post_meta;
        }else{

            if(wp_get_current_user()->ID == 0)
                return;

            $new_post_id = wp_insert_post( array(
                'post_type' => POSTTYPE,
                'post_status' => 'publish',
                'post_title' => wp_get_current_user()->display_name . " - ID: " . wp_get_current_user()->ID,
            ) );

            $meta[$this->user_IP]['logs'] = array(
                'console_errors' => $errors,
                'debug' => "wp_insert_console_errors"
            );

            $meta[$this->user_IP]['screen'] = array(
                'resolution' => array(
                    'width' => "",
                    'height' => ""
                ),
            );

            update_post_meta( $new_post_id, "valcanno_debug_info", $meta );

            return "wp_insert_console_errors";
        }
    }
}