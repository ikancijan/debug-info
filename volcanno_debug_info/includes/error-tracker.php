<?php
/**
 * Volcanno Error tracker
 */

/**
 * Scripts enquue
 */
function vet_enqueue() {
    //wp_enqueue_style('fav-style', plugin_dir_url( __FILE__ ) . 'css/tcs-favorites-style.css', array(), '8.8.18');
    wp_enqueue_script( 'vet-script', PLUGIN_URL . 'assets/js/error-tracker.js', array( 'jquery' ), rand(), true );
    wp_localize_script( 'vet-script', 'volcanno_vet', array(
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'nonce'    => wp_create_nonce( 'volcanno_vet_ajax' ),
    ));
    /* wp_add_inline_script( 'vet-script', '
    alet("asg"); ajhef("rgrrg");
    ' ); */
}
add_action( 'wp_enqueue_scripts', 'vet_enqueue' );

/**
 * Ajax function returns favorites buttons in array
 */
function vet_log_errors() {
    check_ajax_referer( 'volcanno_vet_ajax', 'nonce' );

    $data = isset( $_POST['data'] ) ?  $_POST['data'] : false;

    if( $data ) {

        $debug_info = new VolcannoDebugInfo();
        $debug_info->init();
        wp_send_json_success($debug_info->add_error_tracker_log($data));
    }

    wp_die();
}
add_action( 'wp_ajax_volcanno_vet', 'vet_log_errors' );
add_action( 'wp_ajax_nopriv_volcanno_vet', 'vet_log_errors' );