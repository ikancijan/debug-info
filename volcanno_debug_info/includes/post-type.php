<?php

/**
 * Register post type
 */
function valcanno_create_post_type_debug_info() {

    $labels = array(
        'name' => __('Debug Info', 'volcanno'),
        'singular_name' => __('Debug Info', 'volcanno'),
        'view_item' => __('View Item', 'volcanno'),
        'search_items' => __('Search Item', 'volcanno'),
        'not_found' => __('No Debug Info found', 'volcanno'),
        'not_found_in_trash' => __('No Debug Info found in Trash', 'volcanno'),
    );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 76,
        'menu_icon' => 'dashicons-search',
        'supports' => array('title'),
        'capabilities' => array(
            'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
            'edit_post'          => 'update_core',
            'read_post'          => 'update_core',
            'delete_post'        => 'update_core',
            'edit_posts'         => 'update_core',
            'edit_others_posts'  => 'update_core',
            'delete_posts'       => 'update_core',
            'publish_posts'      => 'update_core',
            'read_private_posts' => 'update_core'
        ),
    );

    register_post_type(POSTTYPE, $args);
}

add_action('init', 'valcanno_create_post_type_debug_info');

/**
 * Scripts enquue
 */
function valcanno_di_enqueue() {
    //wp_enqueue_style('fav-style', plugin_dir_url( __FILE__ ) . 'css/tcs-favorites-style.css', array(), '8.8.18');
    wp_register_script( 'vdi-script', PLUGIN_URL . 'assets/js/debug-info-admin.js', array( 'jquery-ui-accordion' ), '', true );
}
add_action( 'admin_enqueue_scripts', 'valcanno_di_enqueue' );

function wporg_add_custom_box()
{
    add_meta_box(
        'debug_info_box_id',           // Unique ID
        'Debug Info',  // Box title
        'debug_info_box_html',  // Content callback, must be of type callable
        POSTTYPE                   // Post type
    );
}
add_action('add_meta_boxes', 'wporg_add_custom_box');

function debug_info_box_html($post){

    wp_enqueue_script( 'jquery-ui-accordion' );
    wp_enqueue_script( 'vdi-script' );

    $debug_info = new VolcannoDebugInfo();

    $post_meta = (array) get_post_meta( $post->ID, "valcanno_debug_info", false );
    /* echo '<pre>';
    print_r($post_meta[0]);
    echo '</pre>'; */

    echo '<div id="accordion">';

    foreach($post_meta[0] as $ip => $meta){
       
        $detect = $debug_info->set_browser_detect($meta['user_agent']);
        echo '<h3 class="accordion">' . $ip . ' - ' . $meta['time'] . '</h3>
        <div class="panel">
            <div class="inner_accordion">
                <h4 class="accordion_child">User Environment</h4>
                <div class="panel">
                    <table class="debung_info_table">';
                    foreach($detect as $label => $data){
                        echo '<tr>';
                        echo '<th>' . $label . '</th>';
                        echo '<td>' . $data . '</td>';
                        echo '</tr>';
                    }
                    echo '<tr>
                        <th>Country code</th>
                        <td>' . $meta['country_code'] . '</td>
                    </tr>
                    </table>
                </div>
                <h4 class="accordion_child">Console Errors</h4>
                <div class="panel">';
                if(isset($meta['logs']['console_errors'])){
                echo '<table class="debung_info_table">
                        <tr>
                            <th>Error</th>
                            <th>URL</th>
                            <th>Line</th>
                            <th>Time</th>
                        </tr>';
                    foreach($meta['logs']['console_errors'] as $data){
                        echo '<tr>';
                        echo '<td>' . $data['err'] . '</td>';
                        echo '<td>' . $data['url'] . '</td>';
                        echo '<td>' . $data['line'] . '</td>';
                        echo '<td>' . $data['time'] . '</td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                }else{
                    echo '<p>' . _x('No errors', 'volcanno') . '</p>';
                }
                echo '</div>
            </div>
        </div>';
    }

    echo '</div>';
    echo '<style>
            .debung_info_table{display: block;}
            .debung_info_table th{text-align:right}
            .debung_info_table td{padding-left: 1em}
            #publish{display:none!important}

            .accordion.ui-accordion-header{
                background-color: whitesmoke;
                padding: 15px;
            }

            .accordion_child.ui-accordion-header{
                background-color: whitesmoke;
                padding: 10px;
                margin: 10px;
            }

            .ui-accordion-header.ui-state-active{
                background-color: #e4e4e4;
            }

            .inner_accordion .ui-accordion-content.panel{
                padding: 0 15px;
            }
        </style>';

}