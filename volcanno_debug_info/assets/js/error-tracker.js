jQuery.getScript( volcanno_vet.vendor + "DetectZoom.js", function () {
    console.log("DetectZoom.js loaded.");
});

var et_log = [];

et_log.errors = [];
et_log.screen = {
    'width': screen.width,
    'height': screen.height
};

et_log.viewport = {
    'width': document.documentElement.clientWidth,
    'height': document.documentElement.clientHeight
};

window.onerror = function myErrorHandler(err, url, line) { 
    et_log.errors.push({
        'err': err,
        'url': url,
        'line': line,
        'time': new Date()
    });
    // console.log(et_log);
   return false; // so you still log errors into console
}

jQuery(window).bind("load", function () {
    vet_log_errors();
});

function vet_log_errors(log) {
    // console.log("vet_log_errors");
    // console.log(et_log);

    jQuery.ajax({
        url: volcanno_vet.ajax_url,
        method: 'POST',
        data: {
            action: 'volcanno_vet',
            data: et_log,
            nonce: volcanno_vet.nonce
        },
        success: function (response) {
            if (response.success) {
                // console.log("saved");
            }
        },
        error: function () {
            // console.log("vet_log_view error");
        }
    });
}