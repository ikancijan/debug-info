<?php
/*
   Plugin Name: Volcanno Debug Info
   Version: 1.1
   Author: Pixel Industry
   Author URI: http://pixel-industry.com/
   Text Domain: volcanno
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define("POSTTYPE", "v_debug_info");
define("PLUGIN_DIR", plugin_dir_path( __FILE__ ));
define("PLUGIN_URL", plugin_dir_url( __FILE__ ));

// require custom post type
require('includes/post-type.php');

// require debug class
include('includes/VolcannoDebugInfo.class.php');

// require error tracker
include('includes/error-tracker.php');

function init_debug_class(){

    if(is_user_logged_in()){
        $debug_info = new VolcannoDebugInfo();
        $debug_info->init();
    }
}
add_action( 'init', 'init_debug_class' );